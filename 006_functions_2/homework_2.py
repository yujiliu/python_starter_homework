def cleaner(text):
    complete = ''
    for i in range(len(text)):
        if text[i] == ' ' or text[i] == '.' or text[i] == ',':
            continue
        else:
            complete += text[i]
    return complete


def reverse(text):
    reversed = ''
    for i in text:
        reversed += i
    return reversed


def main():
    phrase_real = str(input("Enter text: "))
    phrase_clean = cleaner(phrase_real)
    phrase_reversed = reverse(phrase_clean)
    if phrase_reversed.lower() == phrase_clean.lower():
        print("Palindrome")
    else:
        print("Not palindrome")


if __name__ == '__main__':
    main()
