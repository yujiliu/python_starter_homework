def var(stair):
    if stair == 0 or stair == 1:
        return 1
    else:
        return 1 + var(stair - 2)


def main():
    num = int(input("Please enter number of stair: "))
    print(var(num))


if __name__ == '__main__':
    main()
