def sum_ (num1, num2):
    if num2 == num1:
        return num1
    else:
        return num2 + sum_(num1, num2 - 1)


def main():
    start = int(input("Enter start number: "))
    end = int(input("Enter end number: "))
    print(sum_(start, end))


if __name__ == '__main__':
    main()
