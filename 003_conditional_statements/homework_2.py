import math

num_x = float(input("Enter x: "))
if -math.pi < num_x < math.pi:
    print(math.cos(num_x))
elif num_x < -math.pi or num_x > math.pi:
    print(num_x)
