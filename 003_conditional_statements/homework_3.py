print("ax^2 + bx + c = 0")
num_a = float(input("Enter num a: "))
num_b = float(input("Enter num b: "))
num_c = float(input("Enter num c: "))

dis = (num_b ** 2) - 4 * num_a * num_c

if dis < 0:
    print("There is no x, cause D < 0")
elif dis == 0:
    print(-(num_b / (2 * num_a)))
elif dis > 0:
    print((-num_b + (dis ** 1 / 2)) / 2 * num_a)
    print((-num_b - (dis ** 1 / 2)) / 2 * num_a)
