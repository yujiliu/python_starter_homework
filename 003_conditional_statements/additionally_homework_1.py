import math

print("Options: +, -, /, *, ^, sinx, cosx, tgx")
print("(type one of avaliable symbols)")
operation = input("Enter symbol: ")
if operation == '+' or operation == '-' or operation == '/' or operation == '*':
    num_a = float(input("Enter first number: "))
    num_b = float(input("Enter second number: "))
    if operation == '+':
        print(num_a + num_b)
    elif operation == '-':
        print(num_a - num_b)
    elif operation == '/':
        print(num_a / num_b)
    elif operation == '*':
        print(num_a * num_b)
elif operation == 'sinx' or operation == 'cosx' or operation == 'tgx' or operation == '^':
    num = float(input("Enter number: "))
    if operation == '^':
        power = float(input("Enter power: "))
        print(pow(num, power))
    elif operation == 'sinx':
        print(math.sin(num))
    elif operation == 'cosx':
        print(math.cos(num))
    elif operation == 'tgx':
        print(math.tan(x))
else:
    print("Something wrong, try again.")
