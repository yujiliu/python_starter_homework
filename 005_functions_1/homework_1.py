def default_name(name):
    if name == '':
        print('Hello Yujin')
    else:
        print('Hello', name)


def main():
    username = input("Enter your name: ")
    default_name(username)


if __name__ == "__main__":
    main()
