def power ():
    while True:
        operation = input("Please enter symbol: ")
        print("(+ or - or * or /)")
        if operation == '+':
            print(sum_())
        elif operation == '-':
            print(dif())
        elif operation == '*':
            print(prod())
        elif operation == '/':
            print(div())
        else:
            print("Wrong symbol!")

        power_prog = input("Continue? Y or N: ")
        if power_prog.lower() == 'n':
            break
        else:
            continue


def enter ():
    num = float(input("Enter number: "))
    return num


def sum_():
    num_1 = enter()
    num_2 = enter()
    return num_1 + num_2


def dif ():
    num_1 = enter()
    num_2 = enter()
    return num_1 - num_2


def prod ():
    num_1 = enter()
    num_2 = enter()
    return num_1 * num_2


def div ():
    num_1 = enter()
    num_2 = enter()
    if num_2 != 0:
        return num_1 / num_2
    else:
        print("You can't divide by ZERO")
        return 'ERROR_TRY_AGAIN'


def main():
    power()


if __name__ == '__main__':
    main()
