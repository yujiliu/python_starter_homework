def func1():
    i = -5
    while i <= 5:
        i = i + 0.5
        print(i ** 2)


def func2():
    i = -5
    while i <= 5:
        i = i + 0.5
        print(i ** 1 / 2)


def main():
    func1()
    func2()


if __name__ == '__main__':
    main()
