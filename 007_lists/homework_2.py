def variant (num):
    var = []
    for i in range(0, num, 2):
        var.append(num - i)
    return var


def main():
    stair = int(input("Enter stair number: "))
    if  stair == 0:
        print("¯\_(ツ)_/¯")
        return
    print("There is ", len(variant(stair)), 'variants to go on', stair,'\'s stair')


if __name__ == '__main__':
    main()
