def main():
    length = int(input("Enter length of list: "))
    list = []
    for i in range(length):
        list.append(input("Enter number: "))
    print(max(list))
    print(min(list))
    sum = 0
    for i in range(len(list)):
        sum += int(list[i])
    print(sum)
    print(sum / len(list))


if __name__ == '__main__':
    main()
