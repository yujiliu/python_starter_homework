def listcreator():
    list_def = []
    length_list = int(input("Enter length of list: "))
    for i in range(length_list):
        list_def.append(float(input("Enter number: ")))
    return list_def


def factorial_minus_one(num):
    factorial = 1
    for i in range(1, int(num)):
        factorial = factorial * (num - i)
    return factorial


def easylist(userlist):
    easy_local = []
    for i in range(len(userlist)):
        if userlist[i] > 1:
            factorial = factorial_minus_one(userlist[i])
            if (factorial + 1) % userlist[i] == 0:
                easy_local.append(userlist[i])
            else:
                continue
        else:
            continue
    return easy_local


def easy_pro(userlist):
    pro = 1
    for i in range(len(userlist)):
        pro *= userlist[i]
    return pro


def main():
    easy = []
    list_new = listcreator()
    easy = easylist(list_new)
    print("You create list with this numbers: ", list_new)
    print("And here is prime numbers in it: ", easy)
    print()
    while True:
        power = int(input("Enter 1 - for sum of prime numbers; 2 - to product prime numbers: "))
        if power == 1:
            print("Sum of this prime numbers is: ", sum(easy[0::]))
        elif power == 2:
            print("Product of this prime numbers is: ", easy_pro(easy))
        else:
            print("Something's going wrong! Please try again.")
            print()
        power_program = input("Continue? Y - to continue; N - to exit")
        if power_program.lower() == 'n':
            print("Bye!")
            break
        else:
            continue

if __name__ == '__main__':
    main()
