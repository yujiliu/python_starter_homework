def main():
    length = int(input("Enter length of list: "))
    list = []
    for i in range(length):
        list.append(input("Enter number: "))
    print(list[::-1])


if __name__ == '__main__':
    main()
