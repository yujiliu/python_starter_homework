import math

circle_radius = int(input("Please enter radius of circle: "))
circle_area = math.pi * circle_radius ** 2
print("Area of circle, with radius =", circle_radius, " is: ", circle_area)
