# PYTHON DEVELOPER HOMEWORK

---
[![logo cbs](https://edu.cbsystematics.com/assets/img/site/cbs-logo-black.svg)](https://edu.cbsystematics.com/ua)
---
### Module #1 "Python Starter"
---
#### Lesson #1 "Introduction to Python"

##### Additionally homework #1 & #2
Create a new project in the PyCharm IDE. Create a source code file and write a program that prints out your name. Start it up. Create a second code file and write a program that asks the user for their name and greets them. Start it up. Switch to the first script and run it. Switch back to the second script.

##### Homework #1
Install Python and PyCharm (or whatever Python-enabled IDE you are comfortable with). Explore the settings, customize the development environment for yourself: choose the color scheme and font of the editor that you like, enable or disable displaying line numbers, highlighting the current line, displaying separators between code sections, etc.

##### Homework #2
Create a Python script using a regular text editor (you can use a code editor such as Sublime Text). Run it using the console. Start it by double clicking in Windows Explorer. Use what you've learned so far to figure out how to make sure the console window doesn't close immediately after double-clicking the script.

##### Homework #3
Open IDLE (on Windows and OS X, this app is installed with Python). Experiment with common arithmetic expressions. Try giving a name to the value of some expression. Try to print the value of an expression with explanatory text using the print function, using both the names and the expressions themselves as function parameters.

---
#### Lesson #2 "Variables and data types"

##### Additionally homework #1
Write a program that asks the user for the radius of a circle and displays its area. The formula for the area of a circle is S = 𝜋𝑟2.

##### Homework #1
Write a program that asks the user for two words and outputs them separated by commas.

##### Homework #2
Write a program that asks for three integers a, b, and x and prints True if x is between a and b, or False otherwise.

##### Homework #3
Write a program that solves the quadratic equation 𝑎𝑥2 + 𝑏𝑥 + 𝑐 = 0 using the formulas 𝑥1,2 = −𝑏 ± √𝑏2−4𝑎𝑐2𝑎. The values a, b and c are entered from the keyboard. To extract the root, use the exponentiation operator rather than the math.sqrt function to get complex numbers when the radical expression is negative.

---
#### Lesson #3 "Conditional statements"

##### Additionally homework #1
Write a calculator program where the user can enter, select an operation, enter the required numbers, and get the result. Operations that need to be implemented: addition, subtraction, multiplication, division, exponentiation, sine, cosine and tangent of a number.

##### Homework #1
Write a program that asks the user for his name, and if it matches yours, it displays a specific message.

##### Homework #2
Write a program that calculates the value of a function.

##### Homework #3
Write a program that solves the quadratic equation 𝑎𝑥2 + 𝑏𝑥 + 𝑐 = 0 in real numbers. Unlike a similar exercise from the previous lesson, the program should display a message about the absence of real roots, if the value of the discriminant 𝐷 = 𝑏2−4𝑎𝑐 is negative, the only solution is 𝑥 = −𝑏2𝑎, if it is zero, or two roots 𝑥1,2 = −𝑏 ± √𝐷2𝑎 if it is positive.

---
#### Lesson #4 "Loop statements"

##### Additionally homework #1
Create a program that draws a rectangle of stars with a user-specified width and height on the screen.

##### Homework #1
Given numbers a and b (a <b). Print the sum of all natural numbers from a to b (inclusive).

##### Homework #2
The factorial of the number n is the number 𝑛! = 1 ∙ 2 ∙ 3 ∙… ∙ 𝑛. Create a program that calculates the factorial of a user-entered number.

##### Homework #3
Using nested loops and the functions print (‘*’, end = ’’), print (‘’, end = ’’) and print () print the right triangle.

---
#### Lesson #5 "Functions 1"

##### Additionally homework #1
Create a program that consists of a function that takes three numbers and returns their arithmetic mean, and a main loop that asks the user for a number and calculates their average using the function you just created.

##### Homework #1
Create a function that displays a greeting for the user with the given name. If no name is specified, it should display a greeting for the user with your name.

##### Homework #2
Create two functions that calculate the values of certain algebraic expressions. Display a table of the values of these functions from -5 to 5 in increments of 0.5.

##### Homework #3
Create a calculator program that supports four operations: addition, subtraction, multiplication, division. All data must be entered in a loop until the user indicates that he wants to terminate the execution of the program. Each operation must be implemented as a separate function. The division function should check the data for correctness and issue an error message if it tries to divide by zero.

---
#### Lesson #6 "Functions 2"

##### Additionally homework #1
Write a recursive function that calculates the sum of natural numbers that fall within a given range.

##### Homework #1
Read the Python documentation for information on the standard functions listed in this lesson summary. Check them out in practice.

##### Homework #2
Create a program that checks if the entered phrase is a palindrome.

##### Homework #3
Suppose you can stand on each rung of the ladder with the previous one or by stepping over one. Determine how many ways you can climb a given step.

---
#### Lesson #7 "Lists"

##### Additionally homework #1
Create a list, enter the number of its elements and the values themselves, print these values to the screen in reverse order.

##### Homework #1
Create a list and enter its values. Find the largest and smallest element in the list, as well as the sum and the arithmetic mean of its values.

##### Homework #2
Assignment 2
Rewrite the solution to the last problem from the sixth lesson so that it does not use recursion and does not calculate all intermediate numbers of paths many times (which is extremely inefficient), but stores them in a list.

##### Homework #3
A prime is a number that is divisible only by one and itself. The number 1 is not considered simple. Write a program that finds all prime numbers in a given range, displays them on the screen, and then displays their sum or product at the user's request.
